import { LoggerService } from "@nestjs/common";
import { CandleDataInterface } from "@cryptex-labs/common-candle";
import { CandleSubscription } from "@cryptex-labs/api-ts";
import { CandleUpdateInterface } from "./subscribe";
export declare class CandleGrpcService {
    private readonly logger;
    private readonly apiKey;
    private apiService;
    constructor(logger: LoggerService, host: string, port: number, apiKey: string);
    private static _getOptions;
    getCandlesForGranularitiesAndRange(start: number, end: number, granularities: number[], exchangeId: string, pairId: string): Promise<CandleDataInterface[]>;
    subscribeToCandleUpdates(subscriptions: CandleSubscription[], clientNumber: 1 | 2, onUpdate: (update: CandleUpdateInterface) => void): Promise<any>;
}
