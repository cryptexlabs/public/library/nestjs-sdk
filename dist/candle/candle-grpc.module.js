"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const candle_grpc_service_1 = require("./candle-grpc.service");
const common_2 = require("@cryptex-labs/common");
const loggerProvider = {
    provide: "LoggerService",
    useValue: new common_2.CustomLogger("info", process.env.LOG_LEVELS.split(",")),
};
let CandleGrpcModule = class CandleGrpcModule {
};
CandleGrpcModule = __decorate([
    common_1.Module({
        providers: [candle_grpc_service_1.CandleGrpcService, loggerProvider],
        exports: [candle_grpc_service_1.CandleGrpcService],
    })
], CandleGrpcModule);
exports.CandleGrpcModule = CandleGrpcModule;
//# sourceMappingURL=candle-grpc.module.js.map