"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const microservices_1 = require("@nestjs/microservices");
const common_candle_1 = require("@cryptex-labs/common-candle");
const subscribe_1 = require("./subscribe");
const api_proto_1 = require("@cryptex-labs/api-proto");
const grpc_1 = require("grpc");
class CandleGrpcService {
    constructor(logger, host, port, apiKey) {
        this.logger = logger;
        this.apiKey = apiKey;
        const client = microservices_1.ClientProxyFactory.create(CandleGrpcService._getOptions(host, port));
        this.apiService = client.getService("CandleApiController");
    }
    static _getOptions(host, port) {
        return {
            transport: microservices_1.Transport.GRPC,
            options: {
                package: "api",
                protoPath: (api_proto_1.Path.getProtoPath() + "/candle.proto"),
                url: host + ":" + port,
                loader: {
                    arrays: true,
                },
                credentials: {
                    authorization: process.env.CRYPTEX_LABS_API_KEY,
                },
            },
        };
    }
    getCandlesForGranularitiesAndRange(start, end, granularities, exchangeId, pairId) {
        const request = {
            start,
            end,
            granularities,
            exchangeId,
            pairId,
        };
        const metaData = new grpc_1.Metadata();
        metaData.add("authorization", this.apiKey);
        return this.apiService.getCandlesForGranularitiesAndRange(request, metaData).toPromise()
            .then((response) => {
            const candleDatumDeserializer = new common_candle_1.CandleDatumDeserializer(response.data);
            return Promise.resolve(candleDatumDeserializer.getCandleDatum());
        });
    }
    subscribeToCandleUpdates(subscriptions, clientNumber, onUpdate) {
        const request = new (class SubscriptionRequest {
            constructor() {
                this.clientNumber = clientNumber;
                this.subscriptions = subscriptions;
            }
        })();
        const observable = this.apiService.subscribeToCandleUpdates(request);
        return observable.toPromise().then(() => {
            observable.subscribe((update) => {
                onUpdate(new subscribe_1.CandleUpdateDeserializer(update));
            });
            return Promise.resolve();
        });
    }
}
exports.CandleGrpcService = CandleGrpcService;
//# sourceMappingURL=candle-grpc.service.js.map