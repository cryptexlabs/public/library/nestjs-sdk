import { CandleUpdateInterface } from "./candle-update.interface";
import { CandleUpdate } from "@cryptex-labs/api-ts";
import { CandleDataInterface } from "@cryptex-labs/common-candle";
export declare class CandleUpdateDeserializer implements CandleUpdateInterface {
    private readonly data;
    private readonly _candleDatum;
    constructor(data: CandleUpdate);
    getCandleDatum(): CandleDataInterface[];
    getExchangeId(): string;
    getPairId(): string;
}
