"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_candle_1 = require("@cryptex-labs/common-candle");
class CandleUpdateDeserializer {
    constructor(data) {
        this.data = data;
        const datumDeserializer = new common_candle_1.CandleDatumDeserializer(data.data);
        this._candleDatum = datumDeserializer.getCandleDatum();
    }
    getCandleDatum() {
        return this._candleDatum;
    }
    getExchangeId() {
        return this.data.exchangeId;
    }
    getPairId() {
        return this.data.pairId;
    }
}
exports.CandleUpdateDeserializer = CandleUpdateDeserializer;
//# sourceMappingURL=candle-update.deserializer.js.map