import {ClientGrpc, ClientOptions, ClientProxyFactory, Transport} from "@nestjs/microservices";
import {LoggerService} from "@nestjs/common";
import {CandleDataInterface, CandleDatumDeserializer} from "@cryptex-labs/common-candle";
import {
    CandleApiController,
    CandleSubscription,
    CandleUpdate,
    GetCandlesForGranularitiesAndRangeResponse,
    SubscribeToCandleUpdatesRequest,
} from "@cryptex-labs/api-ts";
import {CandleUpdateDeserializer, CandleUpdateInterface} from "./subscribe";
import {Path} from "@cryptex-labs/api-proto";
import {Metadata} from "grpc";

export class CandleGrpcService {

    private apiService: CandleApiController;

    constructor(
        private readonly logger: LoggerService,
        host: string,
        port: number,
        private readonly apiKey: string) {
        const client    = ClientProxyFactory.create(CandleGrpcService._getOptions(host, port)) as unknown as ClientGrpc;
        this.apiService = client.getService<CandleApiController>("CandleApiController");
    }

    private static _getOptions(host: string, port: number): ClientOptions {
        return {

            transport: Transport.GRPC,
            options:   {
                package:     "api",
                protoPath:   (Path.getProtoPath() + "/candle.proto"),
                url:         host + ":" + port,
                loader:      {
                    arrays: true,
                },
                credentials: {
                    authorization: process.env.CRYPTEX_LABS_API_KEY,
                },
            },
        };
    }

    public getCandlesForGranularitiesAndRange(
        start: number,
        end: number,
        granularities: number[],
        exchangeId: string,
        pairId: string): Promise<CandleDataInterface[]> {

        const request = {
            start,
            end,
            granularities,
            exchangeId,
            pairId,
        };

        const metaData = new Metadata();
        metaData.add("authorization", this.apiKey);

        return this.apiService.getCandlesForGranularitiesAndRange(request, metaData).toPromise()
            .then((response: GetCandlesForGranularitiesAndRangeResponse) => {
                const candleDatumDeserializer = new CandleDatumDeserializer(response.data);
                return Promise.resolve(candleDatumDeserializer.getCandleDatum());
            });
    }

    public subscribeToCandleUpdates(
        subscriptions: CandleSubscription[],
        clientNumber: 1 | 2,
        onUpdate: (update: CandleUpdateInterface) => void): Promise<any> {

        const request = new (class SubscriptionRequest implements SubscribeToCandleUpdatesRequest {
            clientNumber: 1 | 2                 = clientNumber;
            subscriptions: CandleSubscription[] = subscriptions;
        })();

        const observable = this.apiService.subscribeToCandleUpdates(request);

        return observable.toPromise().then(() => {

            observable.subscribe((update: CandleUpdate) => {
                onUpdate(new CandleUpdateDeserializer(update));
            });

            return Promise.resolve();
        });
    }
}