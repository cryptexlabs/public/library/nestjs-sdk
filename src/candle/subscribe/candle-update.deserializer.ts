import {CandleUpdateInterface} from "./candle-update.interface";
import {CandleUpdate} from "@cryptex-labs/api-ts";
import {CandleDataInterface, CandleDatumDeserializer} from "@cryptex-labs/common-candle";

export class CandleUpdateDeserializer implements CandleUpdateInterface {

    private readonly _candleDatum: CandleDataInterface[];

    constructor(private readonly data: CandleUpdate) {
        const datumDeserializer = new CandleDatumDeserializer(data.data);
        this._candleDatum       = datumDeserializer.getCandleDatum();
    }

    public getCandleDatum(): CandleDataInterface[] {
        return this._candleDatum;
    }

    public getExchangeId(): string {
        return this.data.exchangeId;
    }

    public getPairId(): string {
        return this.data.pairId;
    }

}