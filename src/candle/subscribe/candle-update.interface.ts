import {CandleDataInterface} from "@cryptex-labs/common-candle";

export interface CandleUpdateInterface {

    getExchangeId(): string;
    getPairId(): string;
    getCandleDatum(): CandleDataInterface[];
}