import {Module} from "@nestjs/common";
import {CandleGrpcService} from "./candle-grpc.service";
import {CustomLogger} from "@cryptex-labs/common";

const loggerProvider = {
    provide:  "LoggerService",
    useValue: new CustomLogger("info", process.env.LOG_LEVELS.split(",")),
};

@Module({
    providers: [CandleGrpcService, loggerProvider],
    exports:   [CandleGrpcService],
})
export class CandleGrpcModule {
}